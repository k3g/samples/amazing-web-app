# amazing-web-app

## CI Variables

- `CLUSTER_URL`
- `CLUSTER_NAME`
- `CERTIFICATE_AUTHORITY_DATA` (type file)
- `CLUSTER_TOKEN`
- `DOCKER_USER` (your docker handle)
- `DOCKER_PASSWORD` (your docker password)

## How to get the values to fill the CI variables

### API (`CLUSTER_URL`)

```shell
export KUBECONFIG=$(k3d get-kubeconfig -n panda)
grep 'server' $KUBECONFIG | awk -F ': ' '{print $2}'
```

> 👋 use this one: `https://kubernetes.default:443`

### Certificate (`CERTIFICATE_AUTHORITY_DATA`)

```shell
grep 'certificate' $KUBECONFIG | awk -F ': ' '{print $2}' | base64 -d 
```

### Token (`CLUSTER_TOKEN`)

```shell
SECRET=$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
TOKEN=$(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}' | base64 -d)
echo $TOKEN
```
